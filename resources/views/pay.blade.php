<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                {{-- <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div> --}}
                <form method="POST" action="{{ route('pay') }}" id="paymentForm">
                    {{ csrf_field() }}
                    <input type="hidden" name="amount" value="5000.19" /> <!-- Replace the value with your transaction amount -->
                    <input type="hidden" name="payment_method" value="both" /> <!-- Can be card, account, both -->
                    <input type="hidden" name="description" value="I Phone X, 100GB, 32GB RAM" /> <!-- Replace the value with your transaction description -->
                    <input type="hidden" name="logo" value="http://brandmark.io/logo-rank/random/apple.png" /> <!-- Replace the value with your logo url -->
                    <input type="hidden" name="title" value="Victor Store" /> <!-- Replace the value with your transaction title -->
                    <input type="hidden" name="country" value="NG" /> <!-- Replace the value with your transaction country -->
                    <input type="hidden" name="currency" value="NGN" /> <!-- Replace the value with your transaction currency -->
                    <input type="hidden" name="email" value="fionotollan@yahoo.com" /> <!-- Replace the value with your customer email -->
                    <input type="hidden" name="firstname" value="Olufemi" /> <!-- Replace the value with your customer firstname -->
                    <input type="hidden" name="color" value="green" >
                    <input type="hidden" name="size" value="big" >
                    <input type="hidden" name="lastname" value="Olanipekun" /> <!-- Replace the value with your customer lastname -->
                    <input type="hidden" name="phonenumber" value="08098787676" /> <!-- Replace the value with your customer phonenumber -->
                    <input type="hidden" name="pay_button_text" value="Complete Payment" /> <!-- Replace the value with the payment button text you prefer -->
                    <input type="hidden" name="ref" value="MY_NAME_5a2a7f270ac98" /> <!-- Replace the value with your transaction reference. It must be unique per transaction. You can delete this line if you want one to be generated for you. -->
                    <input type="submit" value="Submit"  />
                </form>
            </div>
        </div>
    </body>
</html>
