@extends('layouts.client')

@section('content')
<div class="marginalise">
	<div class="galery-wrapper">
        <div class="container">
            <div class="galery-title text-center">
                <h4 class="heading-regular">ALUMNI GALERY</h4>
            </div>
            <div class="galery-content">
            	 <ul>
            	 	@foreach($gallery as $pic)
                    <li class="col-sm-3 col-xs-6">
                        <div class="galery-item">
                            <img src="{{ secure_asset('gallery_images/'.$pic->image) }}" alt="">
                        </div>
                    </li>
                    @endforeach
                </ul>
            <div class="pagination-wrapper text-center">
            {{ $gallery->links() }}
            </div>
            </div>
        </div>
    </div>
</div>
@endsection