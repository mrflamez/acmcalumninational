@extends('layouts.client')

@section('content')
<div class="marginalise">
	<div class="alumni-story">
        <div class="container">
            <h2 class="text-center">Alumni Articles</h2>
            <hr>
            <div class="row">
                @if(!empty($posts))
                	@foreach($posts as $post)
                    <div class="col-sm-6 col-xs-12">
                        <div class="alumni-story-wrapper">
                            <div class="alumni-story-img">
                                <a href="{{ route('articles.show', ['post' => $post]) }}" title=""><img style="max-height: 300px; width: auto" class="img-responsive" src="{{ secure_asset('post_images/'.$post->image) }}" alt=""></a>
                            </div>
                            <div class="alumni-story-content">
                                <h3 class="heading-regular"><a href="{{ route('articles.show', ['post' => $post]) }}">{{ $post->title }}</a></h3>
                                <p class="text-light">{{ strip_tags(substr($post->body, 0, 45)) }}</p>
                                <span class="dates text-light">{{ Carbon\Carbon::parse($post->created_at)->format('F d, Y') }}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
            <div class="pagination-wrapper text-center">
            {{ $posts->links() }}
            </div>
        </div>
    </div>
</div>
@endsection