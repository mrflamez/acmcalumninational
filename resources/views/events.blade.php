@extends('layouts.client')

@section('content')
<div class="marginalise">
	<div class="event-calendar">
	    <div class="container">
	        <div class="top-section text-center">
	            <h4>All Alumni Events</h4>
	        </div>
			<div style="width: 100%" class="event-list-content">
				@foreach($events as $event)
			    <div class="event-list-item">
			        <div class="date-item">
			            <span class="day text-bold color-theme">{{ Carbon\Carbon::parse($event->event_date)->format('d') }}</span>
			            <span class="dates text-gray text-uppercase">{{ Carbon\Carbon::parse($event->event_date)->format('D') }}</span>
			        </div>
			        <div class="date-desc-wrapper">
			            <div class="date-desc">
			                <div class="date-title"><h4 class="heading-regular"><a href="#">{{ $event->title }}</a></h4></div>
			                <div class="date-excerpt">
			                    <p>{{ strip_tags($event->details) }}</p>
			                </div>
			                <div class="place">
			                    <span class="icon map-icon"></span>
			                    <span class="text-place">{{ $event->venue }} </span>
			                </div>
			            </div>
			        </div>
			        <div class="date-links sold-out text-center">
			           {{--  <a href="#" class="text-regular">SOLD OUT</a> --}}
			           <img style="max-height: 120px" src="{{ secure_asset('event_images/'.$event->image) }}" alt="">
			        </div>
			    </div>
			    @endforeach

            <div class="pagination-wrapper text-center">
            {{ $events->links() }}
            </div>
			</div>
		</div>
	</div>
</div>
@endsection