@extends('layouts.admin')

@section('content')
  <style type="text/css">
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
      position: absolute;
      top: 0;
      right: 0;
      margin: 0;
      padding: 0;
      font-size: 20px;
      cursor: pointer;
      opacity: 0;
      filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }

    img {
      height: 200px !important;
    }
  </style>
	<section class="wrapper">
      	<h3><i class="fa fa-angle-right"></i> Edit Post</h3>
      	
      	<!-- BASIC FORM ELELEMNTS -->
      	<div class="row mt">
      		<div class="col-lg-12">
              <div class="form-panel">
                @if (session('status'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ session('status') }}
                    </div>
                    <br>
                @endif
                @if(count($errors))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
                  <form class="form-horizontal style-form" method="POST" action="{{ route('posts.update', ['post' => $post]) }}"  enctype="multipart/form-data">
                  		{{ csrf_field() }}
                      {{ method_field('PUT') }}
                      <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Post Image</label>
                          <div class="col-sm-10">
                            <div class="input-group image-preview">
                              <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                              <span class="input-group-btn">
                                  <!-- image-preview-clear button -->
                                  <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Clear
                                  </button>
                                  <!-- image-preview-input -->
                                  <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Browse</span>
                                      <input type="file" accept="image/png, image/jpeg, image/gif" name="post_image"/> <!-- rename it -->
                                  </div>
                              </span>
                            </div>
                            <hr>
                              <h3>Old Image</h3>
                              <img style="max-height: 250px" src="{{ secure_asset('post_images/'.$post->image) }}"  class="img-responsive">
                            <hr>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Title</label>
                          <div class="col-sm-10">
                              <input type="text" class="form-control" name="title" value="{{ $post->title }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Body</label>
                          <div class="col-sm-10">
                              <textarea name="body">{{ $post->body }}</textarea>
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary btn-block">Update</button>
                        </div>
                        
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#deleteModal">
                            Delete Post
                          </button>
                        </div>
                        

                      </div>
                  </form>
              </div>
      		</div><!-- col-lg-12-->      	
      	</div><!-- /row -->
      	
	</section>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete Post</h4>
      </div>

      <form action="{{ route('posts.destroy', ['post' => $post]) }}" method="POST">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="modal-body">
        Are you sure you want to delete the post?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Yes</button>
      </div>

      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=m1vs4k0l7f00qlvmimzwyv70m90djmkfwimuvuir45ual6qh'></script>
<script type="text/javascript">
  tinymce.init({
    selector: 'textarea',
    height: 220,
    menubar: false,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor textcolor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table contextmenu paste code help'
    ],
    toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css']
  });
</script>
<script type="text/javascript">
  $(document).on('click', '#close-preview', function(){ 
      $('.image-preview').popover('hide');
      // Hover befor close the preview
      $('.image-preview').hover(
          function () {
             $('.image-preview').popover('show');
          }, 
           function () {
             $('.image-preview').popover('hide');
          }
      );    
  });

  $(function() {
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      // Clear event
      $('.image-preview-clear').click(function(){
          $('.image-preview').attr("data-content","").popover('hide');
          $('.image-preview-filename').val("");
          $('.image-preview-clear').hide();
          $('.image-preview-input input:file').val("");
          $(".image-preview-input-title").text("Browse"); 
      }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
          var img = $('<img/>', {
              id: 'dynamic',
              width:250,
              height:200
          });      
          var file = this.files[0];
          var reader = new FileReader();
          // Set preview image into the popover data-content
          reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          }        
          reader.readAsDataURL(file);
      });  
  });
</script>
@endsection