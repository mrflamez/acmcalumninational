@extends('layouts.admin')

@section('content')
	<section class="wrapper">
	  	<h3><i class="fa fa-angle-right"></i> Pages</h3>
	  		<div class="row mt">
		  		<div class="col-lg-12">
	              <div class="content-panel">
	              	
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                        {{ session('status') }}
	                    </div>
	                    <br>
	                @endif
	                  <section id="unseen">
	                    <table class="table table-bordered table-striped table-condensed">
	                      <thead>
		                      <tr>
		                          <th>Title</th>
		                          <th>Author</th>
		                          <th>Option</th>
		                      </tr>
	                      </thead>
	                      <tbody>
	                      	@foreach($pages as $page)
							<tr>
								<td>{{ $page->title }}</td>
								<td>{{ $page->user->name }}</td>
								<td><a class="btn btn-primary" href="{{ route('pages.edit', ['page' => $page]) }}" >Edit</a></td>
							</tr>
							@endforeach
	                      </tbody>
	                  </table>
	                </section>
	          </div><!-- /content-panel -->
	       </div><!-- /col-lg-4 -->			
	  	</div><!-- /row -->
	</section>	  	
@endsection