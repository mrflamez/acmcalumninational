@extends('layouts.admin')

@section('content')
	<style type="text/css">
		img {
			height: 200px !important;
		}
	</style>
    <link href="{{ secure_asset('admin/js/fancybox/jquery.fancybox.css') }}" rel="stylesheet" />
	<section class="wrapper">
	  	<h3><i class="fa fa-angle-right"></i> Gallery</h3>
  		<div class="row mt">
  			@foreach($gallery as $pic)
  			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc">
				<div class="project-wrapper">
                    <div class="project">
                        <div class="photo-wrapper">
                            <div class="photo">
                            	<a class="fancybox" href="{{ secure_asset('gallery_images/'.$pic->image) }}"><img class="img-responsive" src="{{ secure_asset('gallery_images/'.$pic->image) }}" alt=""></a>
                            </div>
                            <div class="overlay"></div>
                        </div>
                    </div>
                </div>
				<form action="{{ route('gallery.destroy', ['gallery' => $pic]) }}" method="POST">
					<input type="hidden" name="_method" value="DELETE">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button class="btn btn-block btn-danger" type="submit">Delete</button>
				</form>
			</div><!-- col-lg-4 -->
			@endforeach
  		</div><!-- /row -->
      {{ $gallery->links() }}
	</section>	  	
@endsection

@section('scripts')
<script src="{{ secure_asset('admin/js/fancybox/jquery.fancybox.js') }}"></script>
 <script type="text/javascript">
      $(function() {
        //    fancybox
          jQuery(".fancybox").fancybox();
      });

  </script>
  
  <script>
      //custom select box

      $(function(){
          $("select.styled").customSelect();
      });

  </script>

@endsection