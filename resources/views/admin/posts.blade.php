@extends('layouts.admin')

@section('content')
	<style type="text/css">
		img {
			height: 200px !important;
		}
	</style>
	<section class="wrapper">
	  	<h3><i class="fa fa-angle-right"></i> Blog Posts</h3>
	  		<div class="row mt">
		  		<div class="col-lg-12">
	              <div class="content-panel">
	              	
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                        {{ session('status') }}
	                    </div>
	                    <br>
	                @endif
	                  <section id="unseen">
	                    <table class="table table-bordered table-striped table-condensed">
	                      <thead>
		                      <tr>
		                          <th>Title</th>
		                          <th>Author</th>
		                          <th>Option</th>
		                      </tr>
	                      </thead>
	                      <tbody>
	                      	@foreach($posts as $post)
							<tr>
								<td>{{ $post->title }}</td>
								<td>{{ $post->user->name }}</td>
								<td><a class="btn btn-primary" href="{{ route('posts.edit', ['post' => $post]) }}" >Edit</a></td>
							</tr>
							@endforeach
	                      </tbody>
	                  </table>

	                  {{ $posts->links() }}
	                </section>
	          </div><!-- /content-panel -->
	       </div><!-- /col-lg-4 -->			
	  	</div><!-- /row -->
	</section>	  	
@endsection