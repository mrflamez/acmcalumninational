@extends('layouts.admin')

@section('content')
	<style type="text/css">
		.image-preview-input {
		    position: relative;
		    overflow: hidden;
		    margin: 0px;    
		    color: #333;
		    background-color: #fff;
		    border-color: #ccc;    
		}
		.image-preview-input input[type=file] {
		  position: absolute;
		  top: 0;
		  right: 0;
		  margin: 0;
		  padding: 0;
		  font-size: 20px;
		  cursor: pointer;
		  opacity: 0;
		  filter: alpha(opacity=0);
		}
		.image-preview-input-title {
		    margin-left:2px;
		}
	</style>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<section class="wrapper">
      	<h3><i class="fa fa-angle-right"></i> Add new Event</h3>
      	
      	<!-- BASIC FORM ELELEMNTS -->
      	<div class="row mt">
      		<div class="col-lg-12">
              <div class="form-panel">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    <br>
                @endif
                @if(count($errors))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
                  <form class="form-horizontal style-form" method="post" action="{{ route('events.update', ['event' => $event]) }}"  enctype="multipart/form-data">
                  		{{ csrf_field() }}
                      {{ method_field('PUT') }}
                      <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Event Image</label>
                          <div class="col-sm-10">
                            <div class="input-group image-preview">
                              <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                              <span class="input-group-btn">
                                  <!-- image-preview-clear button -->
                                  <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Clear
                                  </button>
                                  <!-- image-preview-input -->
                                  <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Browse</span>
                                      <input type="file" accept="image/png, image/jpeg, image/gif" name="image"/> <!-- rename it -->
                                  </div>
                              </span>
                            </div>
                            <hr>
                              <h3>Old Image</h3>
                              <img style="max-height: 250px" src="{{ secure_asset('event_images/'.$event->image) }}"  class="img-responsive">
                            <hr>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Title</label>
                          <div class="col-sm-10">
                              <input type="text" class="form-control" name="title" value="{{ $event->title }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Venue</label>
                          <div class="col-sm-10">
                              <input type="text" class="form-control" name="venue" value="{{ $event->venue }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Date</label>
                          <div class="col-sm-10">
                              <input type="text" id="datepicker" class="form-control" name="date" value="{{ Carbon\Carbon::parse($event->event_date)->format('m/d/Y') }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Time</label>
                          <div class="col-sm-10">
                              <input type="text" class="form-control timepicker" name="time" value="{{ Carbon\Carbon::parse($event->event_date)->format('h:i A') }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Details</label>
                          <div class="col-sm-10">
                              <textarea name="details" class="form-control">{{ $event->details }}</textarea>
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary btn-block">Update</button>
                        </div>
                        
                      </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#deleteModal">
                            Delete Post
                          </button>
                        </div>
                        

                      </div>
                  </form>
              </div>
      		</div><!-- col-lg-12-->      	
      	</div><!-- /row -->
      	
	</section>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete Post</h4>
      </div>

      <form action="{{ route('events.destroy', ['event' => $event]) }}" method="POST">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="modal-body">
        Are you sure you want to delete the event?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Yes</button>
      </div>

      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript">
  $(document).on('click', '#close-preview', function(){ 
      $('.image-preview').popover('hide');
      // Hover befor close the preview
      $('.image-preview').hover(
          function () {
             $('.image-preview').popover('show');
          }, 
           function () {
             $('.image-preview').popover('hide');
          }
      );    
  });

  $(function() {
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      // Clear event
      $('.image-preview-clear').click(function(){
          $('.image-preview').attr("data-content","").popover('hide');
          $('.image-preview-filename').val("");
          $('.image-preview-clear').hide();
          $('.image-preview-input input:file').val("");
          $(".image-preview-input-title").text("Browse"); 
      }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
          var img = $('<img/>', {
              id: 'dynamic',
              width:250,
              height:200
          });      
          var file = this.files[0];
          var reader = new FileReader();
          // Set preview image into the popover data-content
          reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          }        
          reader.readAsDataURL(file);
      });  
  });
	$('.timepicker').timepicker({
	    timeFormat: 'h:mm p',
	    interval: 30,
	    minTime: '5',
	    maxTime: '11:00pm',
	    defaultTime: '11',
	    startTime: '5:00',
	    dynamic: false,
	    dropdown: true,
	    scrollbar: true
	});
</script>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
@endsection