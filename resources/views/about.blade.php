@extends('layouts.client')

@section('content')
<div class="about-us">
    <div class="about-us-title text-center">
        <div class="container">
            <h1 class="heading-bold text-uppercase">About Us</h1>
        </div>
    </div>
    <div class="about-us-content">
        <div class="container">
            <div class="content-wrapper">
            	<h2>Mission Statement</h2>
                <p class="text-light">
                    The African Church Model College Alumni Association encourages and fosters the involvement of past, present and future generations of the college in the life of their alma mater as ambassadors, advocates, advisors, benefactors and educational partners. We are committed to keeping our alumni informed, involved and invested in the alumni community and the future of the African Church Model College by providing a forum of communication, event, benefits and services for our current and future alumni.
                </p>
                <h2>Vision</h2>
                <p class="text-light">

                   Our vision is to enhance the ability to promote and provide a sense of community and bonding between past, present and future generations of the college and in the life of their alma mater.
                </p>
                <h2>Core Values</h2>
                <p class="text-light">
                	Loyalty, integrity, excellence, respect, and unity are our guiding values.
                </p>
            </div>
        </div>
    </div>
    <!--begin programs & services-->
    <div class="programs-services">
        <div class="container">
            <div class="row">
                <div class="services-img col-md-6 col-sm-12 col-xs-12">
                    <img class="img-responsive" src="{{ secure_asset('images/alumni_exco.jpeg') }}" alt="">
                </div>
                <div class="services-content col-md-6 col-sm-12 col-xs-12">
                    <h2 class="heading-regular">REASONS TO GET INVOLVED WITH ALUMNI ASSOCIATIONS</h2>
                    <p class="text-light" style="font-size: 1.1em; line-height: 1.6em;">
                    	An ALUMNI ASSOCIATION is an association of graduates or, more broadly, of former students (alumni). Alumni Association is a non-profit and non-political organization group that helps to develop a sense of community and bonding between current and former students and staff. It is a group that often supports and provides a forum to form new friendships and business relationships with people of similar background.
                    </p>
                    <br>
                    <p class="text-light" style="font-size: 1.1em; line-height: 1.6em;">
                    	Alunmi associations aren't just charitable organizations though they rely heavily on the gifts of former students, but they also offer former students a wealth of opportunities, so here are just a few of the many reasons to consider joining yours.
                    </p>
                    <br>
                    <div id="tab_services">
                        <!--Nav tabs-->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a class="text-light" href="#social" aria-controls="social" role="tab" data-toggle="tab">Networking opportunities</a>
                            </li>
                            <li role="presentation">
                                <a class="text-light" href="#professional" aria-controls="professional" role="tab" data-toggle="tab">Career building tools</a>
                            </li>
                            <li role="presentation" >
                                <a class="text-light" href="#intelectual" aria-controls="intelectual" role="tab" data-toggle="tab">Give back</a>
                            </li>
                        </ul>
                        <!--Tab panes-->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active animated zoomIn" id="social">
                                <div class="tab-content-wrapper">
                                    <p class="text-light">
                                        One of the main purposes of alumni associations is to support a network of former graduates who will, in turn, help to raise the profile of the institution. Alumni associations aim to bring together like-minded individuals and programs are open to all graduates and offer a broader networking scope. 
                                    </p>
                                    {{-- <ul class="list-item text-light">
                                        <li>Feugiat nulla facilisis at vero eros et accumsan et iusto.</li>
                                        <li>Luptatum zzril delenit augue duis dolore.</li>
                                        <li>Vulputate velit esse molestie consequat.</li>
                                        <li>Delenit augue duis dolore vulputate velit esse molestie consequat</li>
                                    </ul> --}}
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane animated zoomIn" id="professional">
                                <div class="tab-content-wrapper">
                                    <p class="text-light">
                                        Some alumni associations offer a variety of career services, job fairs to things like resume workshops, job postings, and online resources for job-seekers, mentor programs. These can be great tools for building your career or finding ways to maximize your earning potential.
                                    </p>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane animated zoomIn" id="intelectual">
                                <div class="tab-content-wrapper">
                                	<p class="text-light">
                                		Alumni association isn't just about discounts and job offers, most institutions hope that their students' successes post-graduation will promote the school's reputation and encourage others to consider admission. Many alumni association award scholarships (funded by donations from alumni). Many institutions depend on their alumni to spread the word, and alumni recommendations carry a lot of weight with prospective students. So whether you sign up for membership, send a generous donation, or offer to serve as a mentor, there are many ways that your alumni association will help you help your school.

                                	</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end programs & services-->
</div>
@endsection
