@extends('layouts.client')

@section('content')
    <!--begin slider-->
        <div class="slider-hero">
            <div class="sliders-wrap columns1">
                <div class="item">
                    <img src="{{ secure_asset('images/alumni1.jpg') }}" alt="">
                    <div class="owl-caption">
                        <div class="container">
                            <div class="content-block">
                                <h2 class="text-center">
                                    {{-- <span class="text-bold">Hearty Welcomes with </span> <br />
                                    <span class="text-white">a Touch of Rivalry</span> --}}
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="{{ secure_asset('images/alumni2.jpg') }}" alt="">
                    <div class="owl-caption">
                        <div class="container">
                            <div class="content-block">
                                {{-- <h2>
                                    <span class="text-bold">Hearty Welcomes with </span> <br />
                                    <span class="text-white">a Touch of Rivalry</span>
                                </h2> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="{{ secure_asset('images/alumni3.jpg') }}" alt="">
                    <div class="owl-caption">
                        <div class="container">
                            <div class="content-block">
                                {{-- <h2>
                                    <span class="text-bold">Hearty Welcomes with </span> <br />
                                    <span class="text-white">a Touch of Rivalry</span>
                                </h2> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end slider-->
        @if(!empty($latest_event))
        <div class="upcoming-event">
            <div class="container">
                <div class="row">
                    <div class="area-img col-md-5 col-sm-12 col-xs-12">
                        <img style="max-height: 300px" class="img-responsive animated zoomIn" src="{{ secure_asset('event_images/'.$latest_event->image) }}" alt="">
                    </div>
                    <div class="area-content col-md-7 col-sm-12 col-xs-12">
                        <div class="area-top">
                            <div class="row">
                                <div class="col-sm-10 col-xs-9">
                                    <h5 class="heading-light no-margin animated fadeInRight">UPCOMING EVENT</h5>
                                    <h2 class="heading-bold animated fadeInLeft">{{ $latest_event->title }}</h2>
                                    <span>
                                        <span class="icon map-icon"></span>
                                        <span class="text-place text-light animated fadeInRight">{{ $latest_event->venue }}</span>
                                    </span>
                                </div>
                                <div class="col-sm-2 col-xs-3">
                                    <div class="area-calendar calendar animated slideInRight">
                                        <span class="day text-bold">{{ Carbon\Carbon::parse($latest_event->event_date)->format('d') }}</span>
                                        <span class="month text-light">{{ Carbon\Carbon::parse($latest_event->event_date)->format('M') }}</span>
                                        <span class="year text-light bg-year">{{ Carbon\Carbon::parse($latest_event->event_date)->format('Y') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="area-bottom">
                            <div id="time" class="pull-left animated slideInLeft timer"><div class="timer-head-block"></div><div class="timer-body-block"><p style="font-size: 1.2em;">The countdown is finished!</p></div><div class="timer-foot-block"></div></div>
                            {{-- <a href="#" class="bnt bnt-theme join-now pull-right animated fadeIn">Join Now</a> --}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="block-links">
            <div class="container">
                <div class="row">
                    <div class="block-news col-md-4 col-sm-12 col-xs-12">
                        <div class="column-news">
                            <div class="title-links">
                                <h3 class="heading-regular">Latest News</h3>
                            </div>
                            @foreach($posts as $post)
                            <div class="post-wrapper">
                                <div class="post-item clearfix ">
                                    <div class="image-frame post-photo-wrapper">
                                        <a href="{{ route('articles.show', ['post' => $post]) }}"> <img style="max-height: 90px; width: auto;" src="{{ secure_asset('post_images/'.$post->image) }}" alt=""></a>
                                    </div>
                                    <div class="post-desc-wrapper">
                                        <div class="post-desc">
                                            <div class="post-title"><h6 class="heading-regular"><a href="{{ route('articles.show', ['post' => $post]) }}">{{ $post->title }}</a></h6></div>
                                            <div class="post-excerpt">
                                                <p>{{ strip_tags(mb_substr($post->body,0,20,'UTF-8')) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="view-all"><a href="{{ route('articles') }}">View All Articles</a></div>
                        </div>
                    </div>
                    <div class="block-career col-md-4 col-sm-12 col-xs-12">
                        <div class="column-career">
                            <div class="title-links">
                                <h3 class="heading-regular">Gallery</h3>
                            </div>
                            <div class="career-content">
                                <div class="row">
                                @foreach($gallery as $pic)
                                    <div class="col-xs-3">
                                        <img class="img-responsive"  src="{{ secure_asset('gallery_images/'.$pic->image) }}" alt="">
                                    </div>
                                @endforeach
                                </div>
                            </div>
                            <div class="view-all"><a href="{{ route('gallery') }}">View Gallery</a></div>
                        </div>

                    </div>
                    <div class="block-event-calendar col-md-4 col-sm-12 col-xs-12">
                        <div class="column-calendar">
                            <div class="title-links">
                                <h3 class="heading-regular">Event Calendar</h3>
                            </div>
                            <div class="content-calendar bg-calendar no-padding">
                                <div class="top-section">
                                    <h6 class="heading-light">Events</h6>
                                    <span class="icon calendar-icon pull-right"></span>
                                </div>

                                    
                                <div class="list-view">
                                    @foreach($events as $event)
                                    <div class="view-item">
                                        <div class="date-item">
                                            <span class="dates text-light">{{ Carbon\Carbon::parse($event->event_date)->format('D') }}</span>
                                            <span class="day text-bold color-theme">{{ Carbon\Carbon::parse($event->event_date)->format('d') }}</span>
                                            <span class="month text-light">{{ Carbon\Carbon::parse($event->event_date)->format('M') }}</span>
                                        </div>
                                        <div class="date-desc-wrapper">
                                            <div class="date-desc">
                                                <div class="date-title"><h6 class="heading-regular">{{ $event->title }}</h6></div>
                                                <div class="date-excerpt">
                                                    <p>Organizer: Sample Alumni Association</p>
                                                </div>
                                                <div class="place">
                                                    <span class="icon map-icon"></span>
                                                    <span class="text-place">{{ $event->venue}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="view-all"><a href="{{ route('events') }}">View All Events</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

{{-- <!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>
                <div id="app">
                    <example></example>
                </div>
                

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
    
    <script src="{{ secure_asset('js/app.js') }}"></script>
</html> --}}
