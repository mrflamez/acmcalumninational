
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from sayidan_h1.kenzap.com/homepage-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Nov 2017 10:16:33 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
   <link rel="icon" href="favicon.ico" type="image/ico" />
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/styles.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/meanmenu.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <script src="{{ secure_asset('js/libs/modernizr.custom.js') }}"></script>
    <title>Alumni</title>
</head>
<body>
<div class="main-wrapper">
    <!--Begin header ưrapper-->
    <div class="header-wrapper header-position">
        <header id="header" class="container-header type1">
            <div class="top-nav">
                <div class="container">
                    <div class="row">
                        <div class="top-left col-sm-6 hidden-xs">
                            <ul class="list-inline">
                                <li>
                                    <a href="mailto:info@acmcalumninational.com">
                                        <span class="icon mail-icon"></span>
                                        <span class="text">info@acmcalumninational.com</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon phone-icon"></span>
                                        <span class="text">+234 802 342 8392</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="top-right col-sm-6 col-xs-12">
                            <ul class="list-inline">
                                {{-- <li class="top-search">
                                    <form class="navbar-form search no-margin no-padding">
                                        <input type="text" name="q" class="form-control input-search" placeholder="search..." autocomplete="off">
                                        <button type="submit" class="lnr lnr-magnifier"></button>
                                    </form>
                                </li> --}}
                                <li class="login">
                                    <a href="{{ url('/login') }}">Log In</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-middle">
                <div class="container">
                    <div class="logo hidden-sm hidden-xs">
                        <a href="{{ url('/') }}"> <img src="{{ secure_asset('images/logo.png') }}" alt="logo"></a>
                    </div>
                    <div class="menu">
                        <nav>
                            <ul class="nav navbar-nav">
                                <li class="{{ active(['about'], 'current') }}">
                                    <a href="{{ route('about') }}">ABOUT US</a>
                                </li>
                                <li class="{{ active(['events'], 'current') }}">
                                    <a href="{{ route('events') }}">PROGRAM &amp; EVENTS</a>
                                </li>

                                <li  class="{{ active(['articles/*'], 'current') }} {{ active(['articles'], 'current') }}">
                                   <a href="{{ route('articles') }}">ALUMNI ARTICLES</a>
                                </li>
                                <li class="{{ active(['gallery'], 'current') }}">
                                    <a href="{{ route('gallery') }}">GALLERY</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="area-mobile-content visible-sm visible-xs">
                        <div class="logo-mobile">
                            <a href="{{ url('/') }}"> <img src="{{ secure_asset('images/logo.png') }}" alt="logo"></a>
                        </div>
                        <div class="mobile-menu ">
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <!--End header wrapper-->

    <!--Begin content wrapper-->
    <div class="content-wrapper">
       @yield('content')
    </div>
    <!--End content wrapper-->
    <!--Begin footer wrapper-->
    <div class="footer-wrapper type2">
        <footer class="foooter-container">
            <div class="container">
                <div class="footer-middle">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-xs-12 animated footer-col">
                            <div class="contact-footer">
                                <div class="logo-footer">
                                    <a href="{{ url('/') }}"><img src="{{ secure_asset('images/logo.png') }}" alt=""></a>
                                </div>
                                {{-- <div class="contact-desc">
                                    <p class="text-light">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare.</p>
                                </div> --}}
                                <div class="contact-phone-email">
                                    <span class="contact-phone"><a href="#">+234 802 342 8392</a> </span>
                                    <span class="contact-email"><a href="mailto:info@acmcalumninational.com">info@acmcalumninational.com</a></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12  col-xs-12 animated footer-col">
                            {{-- <div class="links-footer">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <h6 class="heading-bold">DASHBOARD</h6>
                                        <ul class="list-unstyled no-margin">
                                            <li><a href="register-page.html">REGISTER</a></li>
                                            <li><a href="career-opportunity.html">CAREER</a></li>
                                            <li><a href="alumni-story.html">STORY</a></li>
                                            <li><a href="alumni-directory.html">DIRECTORY</a></li>
                                        </ul>
                                    </div>
                                    
                                    <div class="col-sm-4 col-xs-12">
                                        <h6 class="heading-bold">ABOUT US</h6>
                                        <ul class="list-unstyled no-margin">
                                            <li><a href="event-single.html">EVENTS</a></li>
                                            <li><a href="galery.html">GALLERY</a></li>
                                            <li><a href="homepage-1.html">HOMEPAGE V1</a></li>
                                            <li><a href="homepage-2.html">HOMEPAGE V2</a></li>
                                        </ul>
                                    </div>
                                    
                                    <div class="col-sm-4 col-xs-12">
                                        <h6 class="heading-bold">SUPPORT</h6>
                                        <ul class="list-unstyled no-margin">
                                            <li><a href="job-detail.html">FAQ</a></li>
                                            <li><a href="about-us.html#contacts">CONTACT US</a></li>
                                            <li><a href="blog.html">ORGANIZER</a></li>
                                            <li><a href="blog-single-fullwith.html">SOCIAL</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 animated footer-col">
                            <div class="links-social">
                                <div class="login-dashboard">
                                    {{-- <a href="login-page.html" class="bg-color-theme text-center text-regular">Login Dashboard</a> --}}
                                </div>
                                <ul class="list-inline text-center">
                                    <li><a target="_blank" href="https://twitter.com/AcmcAlumni"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/acmcalumninational/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="https://web.facebook.com/search/top/?q=acmc%20alumninational"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom text-center">
                    <p class="copyright text-light">©{{ date('Y') }} The African Church Model College Alumni Association</p>
                </div>
            </div>
        </footer>
    </div>
    <!--End footer wrapper-->
</div>

<script src="{{ secure_asset('js/libs/jquery-2.2.4.min.js') }}"></script>
<script src="{{ secure_asset('js/libs/bootstrap.min.js') }}"></script>
<script src="{{ secure_asset('js/libs/owl.carousel.min.js') }}"></script>
<script src="{{ secure_asset('js/libs/jquery.meanmenu.js') }}"></script>
<script src="{{ secure_asset('js/libs/jquery.syotimer.js') }}"></script>
<script src="{{ secure_asset('js/libs/parallax.min.js') }}"></script>
<script src="{{ secure_asset('js/libs/jquery.waypoints.min.js') }}"></script>
<script src="{{ secure_asset('js/custom/main.js') }}"></script>
<script>
    jQuery(document).ready(function () {
        $('#time').syotimer({
            year: 2016,
            month: 12,
            day: 7,
            hour: 7,
            minute: 7,
        });
    });
</script>
</body>
</html>