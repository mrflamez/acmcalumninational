<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DASHGUM - FREE Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ secure_asset('admin/css/bootstrap.css') }}" rel="stylesheet">
    <!--external css-->
    <link href="{{ secure_asset('admin/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('admin/css/zabuto_calendar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('admin/js/gritter/css/jquery.gritter.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('admin/lineicons/style.css') }}">    
    
    <!-- Custom styles for this template -->
    <link href="{{ secure_asset('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('admin/css/style-responsive.css') }}" rel="stylesheet">

    <script src="{{ secure_asset('admin/js/chart-master/Chart.js') }}"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo"><b>Alumni Admin</b></a>
            <!--logo end-->
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">

                  <li class="sub-menu">
                      <a href="javascript:;" class="{{ active(['admin/posts/*'], 'active') }} {{ active(['posts.index'], 'active') }}" >
                          <i class="fa fa-book"></i>
                          <span>Blog</span>
                      </a>
                      <ul class="sub">
                          <li class="{{ active(['posts.index'], 'active') }}"><a  href="{{ route('posts.index') }}">Blogs</a></li>
                          <li class="{{ active(['posts.create'], 'active') }}"><a  href="{{ route('posts.create') }}">Add New</a></li>
                      </ul>
                  </li>

                  {{-- <li class="sub-menu">
                      <a href="javascript:;" class="{{ active(['admin/pages/*'], 'active') }}" >
                          <i class="fa fa-book"></i>
                          <span>Pages</span>
                      </a>
                      <ul class="sub">
                          <li class="{{ active(['pages.index'], 'active') }}"><a  href="{{ route('pages.index') }}">Pages</a></li>
                          <li class="{{ active(['pages.create'], 'active') }}"><a  href="{{ route('pages.create') }}">Add New</a></li>
                      </ul>
                  </li> --}}

                  <li class="sub-menu">
                      <a href="javascript:;" class="{{ active(['admin/events/*'], 'active') }} {{ active(['events.index'], 'active') }}" >
                          <i class="fa fa-calendar"></i>
                          <span>Event</span>
                      </a>
                      <ul class="sub">
                          <li class="{{ active(['events.index'], 'active') }}"><a  href="{{ route('events.index') }}">Events</a></li>
                          <li class="{{ active(['events.create'], 'active') }}"><a  href="{{ route('events.create') }}">Add New</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" class="{{ active(['admin/gallery/*'], 'active') }} {{ active(['gallery.index'], 'active') }}" >
                          <i class="fa fa-picture-o"></i>
                          <span>Gallery</span>
                      </a>
                      <ul class="sub">
                          <li class="{{ active(['gallery.index'], 'active') }}"><a  href="{{ route('gallery.index') }}">Gallery</a></li>
                          <li class="{{ active(['gallery.create'], 'active') }}"><a  href="{{ route('gallery.create') }}">Add New</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
        @yield('content')
      </section>

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              {{ date('Y') }}- Flamez
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{{ secure_asset('admin/js/jquery.js') }}"></script>
  <script src="{{ secure_asset('admin/js/jquery-1.8.3.min.js') }}"></script>
  <script src="{{ secure_asset('admin/js/bootstrap.min.js') }}"></script>
  <script class="include" type="text/javascript" src="{{ secure_asset('admin/js/jquery.dcjqaccordion.2.7.js') }}"></script>
  <script src="{{ secure_asset('admin/js/jquery.scrollTo.min.js') }}"></script>
  <script src="{{ secure_asset('admin/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
  <script src="{{ secure_asset('admin/js/jquery.sparkline.js') }}"></script>


  <!--common script for all pages-->
  <script src="{{ secure_asset('admin/js/common-scripts.js') }}"></script>

  @yield('scripts')
  

  </body>
</html>
