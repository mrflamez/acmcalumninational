@extends('layouts.client')

@section('content')
<div class="marginalise">
<div class="blog-content blog-content-fullwidth">
    <div class="container">
        <div class="blog-post-content full-width">
            <!--Blog Post-->
            <div class="blog-post">
                <div class="area-img">
                    <div class="area-content">
                    	<div class="article-title">
                            <h2 class="text-regular text-capitalize">{{ $post->title }}</h2>
                        </div>

                        <div style="padding: 20px; text-align: center;">
                        	<img style="max-height: 300px; width: auto;" src="{{ secure_asset('post_images/'.$post->image) }}" alt="">
                        </div>

                        <div class="desc">
                            <?php echo $post->body; ?>
                        </div>
                        <div class="stats">
                            <span class="clock">
                                <span class="icon clock-icon"></span>
                                <span class="text-center text-light">{{ Carbon\Carbon::parse($post->created_at)->format('d F, Y') }}</span>
                            </span>
                            <span class="user">
                                <span class="icon user-icon"></span>
                                <span class="text-content text-light">Admin</span>
                            </span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection