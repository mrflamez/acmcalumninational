<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use KingFlamez\Rave\Rave;

class PaymentController extends Controller
{
     public function __construct()
     {
        
     }

     public function load()
     {
     	return view('pay');
     }

    public function redirectToGateway()
    {
        $rave = new Rave('kc');
        return $rave->setMetaData(array('metaname' => 'color', 'metavalue' => 'Green'))->setMetaData(array('metaname' => 'size', 'metavalue' => 'Big'))->setRedirectUrl(route('callback'))->initialize();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $rave = new Rave('kc');
        $result = $rave->verifyTransfer(5000.19,"NGN",true);
        //$result = $this->requeryTransaction(request()->txref);

        dd($result);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }

    public function requery()
    {
        $rave = new Rave();
        $result = $rave->requeryTransaction("kc_5a43d7f994795",true);
        
        dd($result);
    }
}

/**
* 
*/
class Wole 
{
	
	
}
