<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')
               ->paginate(15);
        return view('admin.posts')->with(compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.new-post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'title'     => 'required',
            'body'  => 'required',
            'post_image'  => 'image|mimes:jpeg,png,jpg|max:2048|required'
        ]);

        if (!empty(request()->post_image)) {
            $photoName = time().'.'.request()->post_image->getClientOriginalExtension();
            $request->post_image->move(public_path('post_images'), $photoName);
        }

        else{
            $photoName = "";
        }


        Post::create([
            'title'         => $request->title,
            'body'          => $request->body,
            'user_id'       => Auth::user()->id,
            'image'         => $photoName,
            'keywords'      => $request->title." ".$request->body." "
        ]);

        return  redirect()->route('post.index')->with('status', 'Added new post!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('post')->with(compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('admin.edit-post')->with(compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate(request(), [
            'title'     => 'required',
            'body'  => 'required',
            'post_image'  => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        if (!empty(request()->post_image)) {
            $photoName = time().'.'.request()->post_image->getClientOriginalExtension();
            $request->post_image->move(public_path('post_images'), $photoName);

            $post->title = $request->title;
            $post->body = $request->body;
            $post->user_id = Auth::user()->id;
            $post->image = $photoName;
            $post->keywords = $request->title." ".$request->body." ";
            $post->updated_at = date("Y-m-d H:i:s");

            $post->save();

        }

        else{

            $post->title = $request->title;
            $post->body = $request->body;
            $post->user_id = Auth::user()->id;
            $post->keywords = $request->title." ".$request->body." ";
            $post->updated_at = date("Y-m-d H:i:s");

            $post->save();
        }

        return redirect()->route('posts.edit', ['post' => $post])->with('status', 'Post updated!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        unlink(public_path('post_images/'.$post->image));
        $post->delete();

        return redirect()->route('posts.index')->with('status', 'Post deleted!');
    }
}
