<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth', ['except' => ['show']]);
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$gallery = Gallery::orderBy('created_at', 'desc')
               ->paginate(15);
        return view('admin.gallery')->with(compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.new-gallery');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'images.*'  => 'image|mimes:jpeg,png,jpg|max:500|required'
        ]);
        $input=$request->all();
	    $images=array();
	    if($files=$request->file('images')){
	        foreach($files as $file){
	            $nm = $file->getClientOriginalName();
	            $name = strtolower(str_replace(' ', '', $nm));
	            $file->move(public_path('gallery_images'),$name);
	            $images[]=$name;

	            Gallery::create([
		            'image'  => $name
		        ]);
	        }
	    }


	    return redirect()->route('gallery.index')->with('status', 'Pictures uploaded!');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        unlink(public_path('gallery_images/'.$gallery->image));
        $gallery->delete();

        return redirect()->route('gallery.index')->with('status', 'Picture deleted!');
    }
}
