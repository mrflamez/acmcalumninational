<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth', ['except' => ['show']]);
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::orderBy('created_at', 'desc')
               ->paginate(15);
        return view('admin.pages')->with(compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.new-page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'title'     => 'required',
            'body'  => 'required'
        ]);


        Page::create([
            'title'         => $request->title,
            'body'          => $request->body,
            'user_id'       => Auth::user()->id,
            'keywords'      => $request->title." ".$request->body." "
        ]);

        return  redirect()->route('pages.create')->with('status', 'Added new page!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin.edit-page')->with(compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $this->validate(request(), [
            'title'     => 'required',
            'body'  => 'required'
        ]);

        $page->title = $request->title;
        $page->body = $request->body;
        $page->user_id = Auth::user()->id;
        $page->keywords = $request->title." ".$request->body." ";
        $page->updated_at = date("Y-m-d H:i:s");

        $page->save();

        return redirect()->route('pages.edit', ['page' => $page])->with('status', 'Page updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();

        return redirect()->route('pages.index')->with('status', 'Page deleted!');
    }
}

