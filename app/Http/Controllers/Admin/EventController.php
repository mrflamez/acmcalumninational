<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth', ['except' => ['show']]);
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$events = Event::orderBy('created_at', 'desc')
               ->paginate(15);
        return view('admin.events')->with(compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.new-event');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'title'     => 'required',
            'details'  => 'required',
            'venue'  => 'required',
            'date'  => 'required',
            'time'  => 'required',
            'image'  => 'image|mimes:jpeg,png,jpg|max:2048|required'
        ]);

        if (!empty(request()->image)) {
            $photoName = time().'.'.request()->image->getClientOriginalExtension();
            $request->image->move(public_path('event_images'), $photoName);
        }

        else{
            $photoName = "";
        }

        $date = date("Y-m-d H:i:s", strtotime($request->date." ".$request->time));

        $event = new Event;

        $event->title         = $request->title;
        $event->details       = $request->details;
        $event->image         = $photoName;
        $event->venue         = $request->venue;
        $event->event_date    = $date;
        $event->user_id       = Auth::user()->id;

        $event->save();

        return  redirect()->route('events.index')->with('status', 'Added new event!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('admin.edit-event')->with(compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $this->validate(request(), [
            'title'     => 'required',
            'details'  => 'required',
            'venue'  => 'required',
            'date'  => 'required',
            'time'  => 'required'
        ]);
        
        $date = date("Y-m-d H:i:s", strtotime($request->date." ".$request->time));

        if (!empty(request()->image)) {
            $photoName = time().'.'.request()->image->getClientOriginalExtension();
            $request->image->move(public_path('event_images'), $photoName);

        	$event->title = $request->title;
        	$event->image = $photoName;
            $event->event_date = $date;
	        $event->details = $request->details;
	        $event->user_id = Auth::user()->id;
	        $event->venue = $request->venue;
	        $event->updated_at = date("Y-m-d H:i:s");

	        $event->save();
        }

        else{
	        $event->title = $request->title;
	        $event->details = $request->details;
            $event->event_date    = $date;
	        $event->user_id = Auth::user()->id;
	        $event->venue = $request->venue;
	        $event->updated_at = date("Y-m-d H:i:s");

	        $event->save();
    	}

        return redirect()->route('events.edit', ['event' => $event])->with('status', 'Event updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        unlink(public_path('event_images/'.$event->image));
        $event->delete();

        return redirect()->route('events.index')->with('status', 'Event deleted!');
    }
}
