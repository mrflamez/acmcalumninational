<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Post;
use App\Gallery;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.admin');
    }

    public function about()
    {
        return view('about');
    }

    public function home()
    {
        $latest_event = Event::where('event_date', '>', date('Y-m-d H:i:s'))
                       ->orderBy('event_date', 'asc')
                       ->first();

        $events = Event::where('event_date', '>', date('Y-m-d H:i:s'))
               ->orderBy('event_date', 'asc')
               ->take(5)
               ->get();

        $posts = Post::orderBy('created_at', 'desc')
               ->take(5)
               ->get();
        $gallery = Gallery::orderBy('created_at', 'desc')
               ->take(16)
               ->get();

        return view('welcome')->with(compact('latest_event'))->with(compact('events'))->with(compact('posts'))->with(compact('gallery'));
    }

    public function event()
    {
        $latest_event = Event::orderBy('created_at', 'asc')->first();
        $events = Event::where('event_date', '>', date('Y-m-d H:i:s'))
               ->orderBy('event_date', 'asc')
               ->paginate(12);
        return view('events')->with(compact('latest_event'))->with(compact('events'));
    }

    public function article()
    {
        $posts = Post::orderBy('created_at', 'desc')
               ->paginate(9);
        return view('posts')->with(compact('posts'));
    }

    public function gallery()
    {
        $gallery = Gallery::orderBy('created_at', 'desc')
               ->paginate(16);
        return view('gallery')->with(compact('gallery'));
    }
}












