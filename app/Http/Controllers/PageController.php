<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Auth;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        return view('admin.pages')->with(compact('pages'));
    }

    
    public function show(Page $page)
    {
    }
}
