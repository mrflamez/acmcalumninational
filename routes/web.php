<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@home')->name('homepage');
Route::get('/events', 'HomeController@event')->name('events');
Route::get('/gallery', 'HomeController@gallery')->name('gallery');
Route::get('/about-us', 'HomeController@about')->name('about');
Route::get('/articles', 'HomeController@article')->name('articles');
Route::get('/articles/{post}', 'Admin\PostController@show')->name('articles.show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function () {
	Route::resource('posts', 'Admin\PostController', ['except' => [ 'show' ]]);
	//Route::resource('pages', 'Admin\PageController', ['except' => [ 'show' ]]);
	Route::resource('events', 'Admin\EventController', ['except' => [ 'show' ]]);
	Route::resource('gallery', 'Admin\GalleryController', ['except' => [ 'show', 'edit', 'update' ]]);
});